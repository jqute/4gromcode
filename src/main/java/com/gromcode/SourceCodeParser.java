package com.gromcode;

import com.sun.tools.javac.util.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author olesya.daderko
 */
public class SourceCodeParser {
    private static final String CLASS = "class";
    private static final char LEFT_CURLY = '{';
    private static final char RIGHT_CURLY = '}';
    private static final String EXTENDS = "extends";
    private static final String PUBLIC_CLASS_RE = "^\\s*public class (\\w+)";
    private static final String PUBLIC_CLASS_EXTENDS = "^\\s*public class %s extends (\\w+)";

    public Collection<JavaClassObject> parse(String[] source) {
        List<String> classLines = new ArrayList<>();
        Map<String, JavaClassObject> classes = new HashMap<>();

        String className = null;
        int curlyBracesNumber = 0;
        boolean foundExtendsInClass = false;

        for (String line : source) {
            if (line.isEmpty()) continue;

            // first we try to define class name
            if (curlyBracesNumber == 0 && className == null) {
                if (line.contains(CLASS)) {
                    className = findByRegex(PUBLIC_CLASS_RE, line);

                    if (line.contains(EXTENDS))
                        foundExtendsInClass = true;
                }
            }

            // count curly braces
            curlyBracesNumber += line.chars().filter(value -> value == LEFT_CURLY).count();
            curlyBracesNumber -= line.chars().filter(value -> value == RIGHT_CURLY).count();

            // remove extends class if exist
            classLines.add(foundExtendsInClass
                    ? line.substring(0, line.indexOf("extends")) + LEFT_CURLY
                    : line + " ");

            // insert the class body of the parent class if exist
            if (curlyBracesNumber == 1 && foundExtendsInClass) {
                String extendsClass = findByRegex(String.format(PUBLIC_CLASS_EXTENDS, className), line);
                classLines.add(arrayToString(classes.get(extendsClass).getClassBody()));
                foundExtendsInClass = false;
            }

            // if all curly braces are closed, create a class object
            if (curlyBracesNumber == 0 && className != null) {
                classes.put(className,
                        new JavaClassObject(className, classLines.toArray(new String[classLines.size()])));
                classLines = new ArrayList<>();
                className = null;
            }
        }

        return classes.values();
    }

    private String findByRegex(String regex, String line) {
        Pattern classPattern = Pattern.compile(regex);
        Matcher classMatcher = classPattern.matcher(line);
        return classMatcher.find() ? classMatcher.group(1) : null;
    }

    public static String arrayToString(String[] array) {
        StringJoiner stringJoiner = new StringJoiner(" \n");
        Arrays.stream(array).forEach(stringJoiner::add);
        return stringJoiner.toString();
    }

}
