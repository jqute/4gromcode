package com.gromcode;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static com.gromcode.SourceCodeParser.arrayToString;

public class Main {

    public static void main(String[] args) throws IOException {
        String[] sourceCode = new String[]{
                "public class Animal { ",
                "void walk() { ",
                "System.out.println(\"I am walking\"); ",
                "} ",
                "}",
                "",
                "public class Bird extends Animal {",
                "void fly() {",
                "System.out.println(\"I am flying\");",
                "}",
                "}",
        };

        SourceCodeParser parser = new SourceCodeParser();
        Collection<JavaClassObject> classes = parser.parse(sourceCode);
        JavaClassCompiler compiler = new JavaClassCompiler();

        List<Diagnostic<? extends JavaFileObject>> diagnostics = compiler.compile(classes);

        if (diagnostics.isEmpty()) {
            System.out.format("Successfully compiled %d classes.", classes.size());
        }
        else {
            diagnostics.forEach(d -> {
                System.out.format("Error on line %d in %s%n", d.getLineNumber(), d.getSource().toUri());
                System.out.format("Massage: %s", d.getMessage(Locale.ENGLISH));
            });
        }
    }

}
