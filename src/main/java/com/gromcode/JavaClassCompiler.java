package com.gromcode;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.gromcode.SourceCodeParser.arrayToString;

/**
 * @author olesya.daderko
 */
public class JavaClassCompiler {
    private final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();


    /**
     * Compile given java classes.
     *
     * @param classes the classes to compile
     * @return the list of {@link Diagnostic}.
     * @throws IOException
     */
    public List<Diagnostic<? extends JavaFileObject>> compile(Collection<JavaClassObject> classes) throws IOException {
        List<JavaSourceFromString> classList = classes.stream()
                .map(clazz -> new JavaSourceFromString(clazz.getName(), arrayToString(clazz.getClassSource())))
                .collect(Collectors.toList());

        // compile with diagnostic
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);

        compiler.getTask(null, fileManager, diagnostics, null, null, classList).call();

        fileManager.close();

        return diagnostics.getDiagnostics();
    }

}
