package com.gromcode;

/**
 * @author olesya.daderko
 */
public class JavaClassObject {

    private String name;
    private String[] classSource;
    private String[] classBody;

    public JavaClassObject(String name, String[] classSource) {
        this.name = name;
        this.classSource = classSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getClassSource() {
        return classSource;
    }

    public void setClassSource(String[] classSource) {
        this.classSource = classSource;
    }

    /**
     * Returns the body of the class. For example for
     *
     * <pre>
     *     public class Test {
     *         public void print() {
     *             System.out.print("Test.");
     *         }
     *     }
     * </pre>
     *
     * it returns
     *
     * * <pre>
     *         public void print() {
     *             System.out.print("Test.");
     *         }
     * </pre>
     *
     * @return the body of the class.
     */
    public String[] getClassBody() {
        this.classBody = new String[this.classSource.length - 2];
        System.arraycopy(this.classSource, 1, this.classBody, 0, classBody.length);
        return this.classBody;
    }
}
